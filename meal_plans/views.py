from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin

from meal_plans.models import Meal_plan

# Create your views here.

class Meal_planListView(ListView):
    paginate_by = 2
    model = Meal_plan
    template_name = "meal_plans/list.html"


class Meal_planDetailView(DetailView):
    model = Meal_plan
    template_name = "meal_plans/detail.html"


class Meal_planCreateView(LoginRequiredMixin, CreateView):
    model = Meal_plan
    template_name = "meal_plans/new.html"
    fields = ["name", "date", "recipes"]
    success_url = reverse_lazy('meal_plans_list')

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class Meal_planUpdateView(LoginRequiredMixin, UpdateView):
    model = Meal_plan
    template_name = "meal_plans/edit.html"
    fields = ["name", "date", "recipes"]
    success_url = reverse_lazy('meal_plans_list')


class Meal_planDeleteView(LoginRequiredMixin, DeleteView):
    model = Meal_plan
    template_name = "meal_plans/delete.html"
    success_url = reverse_lazy('meal_plans_list')
    