from django.urls import path
from meal_plans.views import (
    Meal_planCreateView,
    Meal_planDeleteView,
    Meal_planDetailView,
    Meal_planListView,
    Meal_planUpdateView
)


urlpatterns = [
    path("", Meal_planListView.as_view(), name="meal_plans_list"),
    path("<int:pk>/", Meal_planDetailView.as_view(), name="meal_plans_detail"),
    path('<int:pk>/delete/', Meal_planDeleteView.as_view(), name='meal_plans_delete'),
    path("<int:pk>/edit/", Meal_planUpdateView.as_view(), name="meal_plans_edit"),
    path("create/", Meal_planCreateView.as_view(), name="meal_plans_new"),
]