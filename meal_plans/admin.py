from django.contrib import admin


from meal_plans.models import Meal_plan

# Register your models here.

class Meal_planAdmin(admin.ModelAdmin):
    pass

admin.site.register(Meal_plan, Meal_planAdmin)